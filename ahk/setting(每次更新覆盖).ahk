﻿;快捷键
global starthothey:="F2"

global reloadhothey:="^F2"

;游戏版本 1国际服汉化   2国服    3国际服原版
global gameVersion:=2

;匹配结束需要的分数
global needPoint:=1

;基底词缀数量 没有写0 有几条写几条
;这个词缀用于计算从   物品等级： 开始到词缀需要多少行，普通情况下写基底词缀即可
;对于有附魔的物品，附魔那2行也需要添加
global Implicit:=2

;自动增幅开关
global AutoAug:=1
;自动增幅需要的分数
global AugPoint:=0

;自动富豪开关
global AutoRegal:=0
;自动富豪需要的分数
global RegalPoint:=2

;自动重铸开关
global AutoScour:=0

;自动混沌开关  需要物品为稀有   1为自动混沌 2为自动重铸点金
global AutoChaos:=0

;自动EX 还没做，不过你可以加钱
global AutoEX:=0

;最大的tips数量，系统内置最大20，超过就报错，建议用默认的5 反正也没啥好看的
global maxTipsCount:=5
;操作间隔，毫秒 默认200ms，不建议太快 我给你随机了一个1-100毫秒的随机值 加上你自己设置的这个 建议默认
global ranbase:=200

;-----别动这2行
global modMatchList :={}
global modFuheList :={}
;-----别动这2行

;开始记录需要匹配的词缀  可以最简单的就写匹配的文字 也可以用正则匹配数值 也可以正则匹配多行 姿势很多看你会不会了
modMatchList["珠宝"] := [1]
modMatchList["魔力"] := [1]
modMatchList["[2-8][0-9] to maximum Mana"] := [1]
modMatchList["该装备的物理伤害提高 ([5][5-9]|[6-7][0-9])%([\w\W]*)([1][2][4-9]|[1][3-9][0-9]|[200]) 命中值"]:=1
;结束


;开始记录复合词缀情况 这个可以不写
;结束