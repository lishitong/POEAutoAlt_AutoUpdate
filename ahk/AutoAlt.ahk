﻿#SingleInstance force
#IncludeAgain %A_MyDocuments%\My Games\Path of Exile\tempSetting.ahk
;安装钩子，检测是否有用户输入
#InstallMouseHook
#InstallKeybdHook
SetWorkingDir %A_ScriptDir%
global Position:= {}
global cW,cH
global LastUseCurrency=""
global TimeString
global flagList:={}
global TipsList := {}
global UsedList := {}
global TempCopy:=["1","2","3"]
Gui,-Caption
Gui, Add, Text,, 双击加载配置文件
Gui, Add, ListBox, vMyListBox gMyListBox w200 h200
loop, %A_ScriptDir%\setting*.AHK ;
	GuiControl,, MyListBox, %A_LoopFileName%
;这里别动--------------------
if 0 >= 2
{
	SplitPath, 2,,,,name_no_ext
	if !FileExist("统计")
		FileCreateDir,统计
	global successLogPath:="统计\" name_no_ext ".txt"
	if !gameVersion
	{
		MsgBox,配置文件错误
		Reload
	}
ms=
(
当前配置:
当前快捷键: 开始.%starthothey%,重新选择配置文件.%reloadhothey%
游戏版本:%gameVersion%
匹配分数:%needPoint%
基底词缀:%Implicit%
自动增幅:%AutoAug%,自动增幅前提:%AugPoint%
自动富豪:%AutoRegal%,自动富豪前提:%RegalPoint%
自动重铸:%AutoScour%
自动混沌:%AutoChaos%
确定加载这个配置?
)
	MsgBox, 4, , %ms%
	IfMsgBox No
	{
		Gui, Show
		return
	}
}
else
{
	Gui, Show
	return
}


;--------------------------------------------------------------------------------------------------------------------
;下面就别改了
;初始化日志文件
FormatTime, TimeString,,yyyy-MM-dd hhmmss
if !FileExist("logs")
	FileCreateDir,logs
FileAppend,开始记录时间:%TimeString%`n, logs\%TimeString%.txt
;初始化坐标
Initialize()

Hotkey,%starthothey%,Startahk
return


;开始
#IfWinActive, ahk_class POEWindowClass

Startahk:
ms=
(
当前配置:
游戏版本:%gameVersion%
匹配分数:%needPoint%
基底词缀:%Implicit%
自动增幅:%AutoAug%,自动增幅前提:%AugPoint%
自动富豪:%AutoRegal%,自动富豪前提:%RegalPoint%
自动重铸:%AutoScour%
自动混沌:%AutoChaos%
停止办法:移动鼠标或者点击键盘 任何动作都可以
如果发现匹配出来的词缀和物品不符请立刻停止,大概率和基底数量有关
)
WinActivate
MsgBox,%ms%
Send, {LShift Down}
Sleep 1200
MouseMove,50,50
MouseMove,Position["物品"].x,Position["物品"].y


;下面的可以不管
while ( A_TimeIdlePhysical>1000)  ; 判断条件 
{
    matchPoint:=0               ; 初始化匹配计数器
    matchFuhe:=0
    msg:=""
    mathpointlist:=[0,0,0]
	
	
	;读取物品信息
    clipboard:=
	loop,10
	{
		Send ^c
		ClipWait,0.2
	} until StrLen(clipboard)
	
	if  !StrLen(clipboard)
	{
		Stop()
		MsgBox,没有读取到物品信息
		return
	}

    ;获取物品稀有度
    RegExMatch(clipboard, flagList["itemRarityMatchText"], itemRarity)
    itemMod:=GetMod(clipboard)  ;读取物品的词缀
    RegExReplace(itemMod, "`n", "", modCount) ;获取物品词缀的行数(没有复合词缀的时候可认为就是词缀个数)

	;检查词缀是否正常
	TempCopy.RemoveAt(1)
	TempCopy.Push(itemMod)
	if(TempCopy[1]==TempCopy[2]==TempCopy[3])
	{
		Stop()
		MsgBox,连续三次词缀相同,检查是否出现问题
	}
	
    For mod, pointList in modMatchList {  ;进行词缀匹配
        RegExReplace(itemMod,mod,"", outCount)
        if (outCount>0){
            for i,point in pointList
            {
                mathpointlist[i]+=Floor(point)
            }
        }
    }
    ;获取匹配到的最大分数
    matchPoint:= Max(mathpointlist*)
    
    ;去掉复合词缀的数量
    For mod, point in modFuheList {  ;匹配复合词缀
        RegExReplace(itemMod,mod,"",outCount)
        if (outCount!=0)
            matchFuhe:=matchFuhe+point
    }
    modCount:=modCount-matchFuhe
    
    ;记录日志 记录你物品洗出的MOD
    FormatTime, logtime,,hh:mm:ss
    msg=%logtime%`r`n词缀`r`n%itemMod%总分:%matchPoint%
    FileAppend,%msg%, logs\%TimeString%.txt
    
    Toolmsg(msg)
    
    ;如果匹配的分数够了 跳出弹窗并停止
    if (matchPoint>=needPoint or ( itemRarity==flagList["rareFLag"] and AutoScour==0 and AutoChaos==0))
    {
		;记录使用的通货情况
		Stop()
		tmsg:=""
		For curName, curCost in UsedList
		{
			tmsg.=curName ":" curCost "   "
		}
		UsedList := {}
		;播放提示音
		SoundPlay,提示音.mp3
		
		;记录结果到日志，方便统计
		FormatTime, logtime,,hh:mm:ss
		t_logmsg=%logtime%`r`n通货使用%tmsg%`r`n%itemMod%`r`n
		FileAppend,%t_logmsg%, %successLogPath%
		
		;弹出提示框
        MsgBox, %tmsg% `n分数 : %matchPoint%`n%clipboard%
        break
    }
    ;稀有物品
    else if (itemRarity==flagList["rareFLag"])
    {
        ;是否自动混沌
        if(AutoChaos==1)
        {
            UseCurrency("混沌石")
        }
		else if(AutoChaos==2)
		{
			UseCurrency("重铸石")
            UseCurrency("点金石")
		}
        ;是否自动重铸
        else if(AutoScour==1)
        {
            ;使用重铸 使用蜕变
            UseCurrency("重铸石")
            UseCurrency("蜕变石")
        }
    }
    ;魔法物品
    else if (itemRarity==flagList["magicFLag"])
    {
        ;自动增幅  需要词缀数为1 分数大于增幅前提
        if (AutoAug==1 and matchPoint>=AugPoint and modCount==1)
        {
            UseCurrency("增幅石")
        }
        ;自动富豪  需要词缀数为2 分数大于富豪前提
        else if(AutoRegal==1 and matchPoint>=RegalPoint and modCount==2)
        {
            UseCurrency("富豪石")
        }
        ;什么都不匹配就自动改造  改造你也不要？那你开这个干JB呢
        else
        {
            UseCurrency("改造石")
        }
    }
    ;间隔 不建议太快
    Random, rand, 1,100
    Sleep (ranbase+rand)
}
Stop()
if(UsedList.Count()>0)
{
	tmsg:=""
	For curName, curCost in UsedList
	{
		tmsg.=curName ":" curCost "   "
	}
	MsgBox, 262404,自动改造 ,%tmsg% `n是否清空已使用通货统计
	IfMsgBox yes
		UsedList := {}
}
return

#IfWinActive

;停止程序
Stop()
{
    if(GetKeyState("LShift"))
        Send, {LShift up}
	LastUseCurrency :=""
}


;读取物品词缀
GetMod(checkstr)
{
	;读取配置文件 查看是否有基底词缀
	itemlvlindex:=InStr(checkstr,flagList["itemLevelFLag"],false) ;匹配起始点
	Occurrence :=Implicit>0?3+Implicit:2
	indexStart:=InStr(checkstr,"`n",false,itemlvlindex,Occurrence)+1 ;继续查找
	indexEnd:=InStr(checkstr,"--------",false,indexStart)
	if(indexEnd!=0)
	{
		itemMod:=SubStr(checkstr, indexStart,indexEnd-indexStart)
	}
    else
    {
        itemMod:=SubStr(checkstr, indexStart)
    }
    return itemMod
}

;使用通货
UseCurrency(currname)
{
	if (currname==LastUseCurrency)
	{
		Send, +{Click}
	}
	else
	{
		Click, up, Right
		MouseMove,Position[currname].x,Position[currname].y
		
		clipboard:=
		loop,10
		{
			Send ^c
			ClipWait,0.2
		} until StrLen(clipboard)
		
		if  !StrLen(clipboard)
		{
			MsgBox,通货不够了,得加钱
			return
		}
		
		Click, Right
		ClickItem()
	}
	;记录使用通货日志
	if(UsedList.HasKey(currname))
		UsedList[currname]+=1
	else
		UsedList[currname]:=1
	FormatTime, logtime,,hh:mm:ss
	FileAppend,`r`n使用%currname%`r`n`r`n, logs\%TimeString%.txt
	LastUseCurrency:=currname
}

;对物品使用
ClickItem()
{
	MouseMove,Position["物品"].x,Position["物品"].y
	Click
}

Toolmsg(msg)
{
	tipsy:=120
	if maxTipsCount<=0
	{
		tmsg:=""
		For curName, curCost in UsedList
		{
			tmsg.=curName ":" curCost "   "
		}
		ToolTip,%tmsg%,% ch/20*12.5,tipsy,20
	}
	else
	{
		;初始化高度和最大数量
		tipsy:=120
	
		;获取物品行数 并添加到数组
		RegExReplace(msg, "`n", "", line)
		line:=line+1
		TipsList.InsertAt(1, {text:msg,line:line})
		
		loop,% TipsList.Count()
		{
			ToolTip,% TipsList[A_Index].text,% ch/20*12.5,tipsy,% A_Index
			tipsy:=tipsy+16*(TipsList[A_Index].line)+12
			if (A_Index==maxTipsCount)
			{
				if (TipsList.Count()>maxTipsCount)
					TipsList.RemoveAt(maxTipsCount+1,TipsList.Count()-maxTipsCount)
				return
			}
		}
	}
	SetTimer,HideTips,-4000
}

HideTips:
TipsList := {}
loop,20
	ToolTip,,,,% A_Index
return

MyListBox:
	if (A_GuiEvent != "DoubleClick")
		return
ButtonOK:
	GuiControlGet, MyListBox  ; 获取列表框中当前选择的项目.
	filepath:=A_MyDocuments "\My Games\Path of Exile\tempSetting.ahk"
	FileCopy,%MyListBox%,%filepath%,1
	t_filename:=MyListBox
    Run  %A_ScriptFullPath% "1" %t_filename% /restart
    ExitApp
return


Initialize()
{
	poewindow := WinExist("ahk_class POEWindowClass")
	if (!poewindow)
	{
		MsgBox,先开游戏
		ExitApp
	}
	WinActivate
	GetClientSize(poewindow, cW, cH)
	CoordMode, Mouse, Relative
	MouseGetPos, mrX, mrY
	CoordMode, Mouse, Client
	MouseGetPos, mcX, mcY
	CoordMode, Mouse,Window
	mt:=mry-mcy
	Position["蜕变石"]:={x: 0.06*ch,y: 0.27*ch+mt}
	Position["改造石"]:={x: 0.11*ch,y: 0.27*ch+mt}
	Position["机会石"]:={x: 0.22*ch,y: 0.27*ch+mt}
	Position["增幅石"]:={x: 0.22*ch,y: 0.32*ch+mt}
	Position["富豪石"]:={x: 0.41*ch,y: 0.27*ch+mt}
	Position["点金石"]:={x: 0.46*ch,y: 0.27*ch+mt}
	Position["混沌石"]:={x: 0.52*ch,y: 0.27*ch+mt}
	Position["重铸石"]:={x: 0.17*ch,y: 0.44*ch+mt}
	Position["物品"] := {x: 0.32*ch,y: 0.44*ch+mt}
	switch gameVersion
	{
		;1 国际服带A大汉化
		case 1:
		flagList:={rareFlag:"稀有",magicFlag:"魔法",itemRarityMatchText:"(?<=\稀有度: ).*",itemLevelFLag:"物品等级"}

		;2 国服
		case 2:
		flagList:={rareFlag:"稀有",magicFlag:"魔法",itemRarityMatchText:"(?<=\稀 有 度: ).*",itemLevelFLag:"物品等级"}
		;3 国际服 英文
		case 3:
		flagList:={rareFlag:"Rare",magicFlag:"Magic",itemRarityMatchText:"(?<=Rarity: ).*",itemLevelFLag:"Item Level"}

	}
}

GetClientSize(hWnd, ByRef w := "", ByRef h := "")
{
	VarSetCapacity(rect, 16)
	DllCall("GetClientRect", "ptr", hWnd, "ptr", &rect)
	w := NumGet(rect, 8, "int")
	h := NumGet(rect, 12, "int")
}
