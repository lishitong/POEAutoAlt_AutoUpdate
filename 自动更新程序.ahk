﻿#SingleInstance force
;提权
if !A_IsAdmin && !%False%
{
    Run *RunAs "%A_AhkPath%" "%A_ScriptFullPath%"
	ExitApp
}

global localVersion
FileEncoding,UTF-8
global title:="自动改造-自动更新程序"
global alipath:="http://ahkforpoebyns.oss-cn-beijing.aliyuncs.com/AutoAlt/"
global mainAhk:="ahk\AutoAlt.ahk"

;1----主程序文件,2----主程序配件,每次检查,3----更新程序文件
global fileList:={"0":"0"
,"AutoAlt.ahk"				:["ahk\AutoAlt.ahk",1]
,"setting(每次更新覆盖).ahk"	:["ahk\setting(每次更新覆盖).ahk",2]
,"提示音.mp3"				:["ahk\提示音.mp3",2]
,"说明书.docx"				:["说明书(4-26更新).docx",2]
,"自动更新程序.ahk"			:[A_ScriptName,3]
,"Version.txt"				:[A_MyDocuments "\My Games\Path of Exile\AultAltVersion.txt",3]
,"更新说明.txt"				:["更新说明.txt",3]
,"小广告.txt"				:["小广告.txt",3]
,"加钱通道-新.png"			:["加钱通道-新.png",3]}
fileList.Delete("0")

;首先的首先的首先 检查注册表，确定你是安装了AHK还是拖到EXE上运行的ahk
CheckReg()

;先更新自己
if(!A_Args[1])
{
	DownloadFileTo("自动更新程序.ahk")
	Run  %A_ScriptFullPath% "1" /restart
	ExitApp
}



;再检查加钱通道是否畅通
if !FileExist(fileList["加钱通道-新.png"][1])
	DownloadFileTo("加钱通道-新.png")

;gui从trade抄的
Gui, New, +LastFound +Border -resize -SysMenu  -Caption +AlwaysOnTop
Gui, Margin, 10, 2
Gui, Color, FFFFFF, 000000

Gui, Add, Progress, w900 h50 x0 y0 c505256 Background505256

Gui, Font, s25 cFFFFFF bold, 黑体
Gui, Add, Text, x10 y5 h50 w450 +Center BackgroundTrans, %title%

Gui, Font, s12 c000000 norm, 黑体
Gui, Add, Text, x10 y+10 w450 r10 +Left BackgroundTrans vSplashSubMessage,

Gui, Add, Picture, x315 y60  w-1 h150 gLaunchPic , 加钱通道-新.png
Gui, Font, s10 c000000, 黑体
Gui, Add, Text, x10 y+5 h20 w450 +Right BackgroundTrans cRed gLaunchPic ,↑↑↑加钱通道↑↑↑

Gui, Font, s10 c000000, 黑体
Gui, Add, Text, x10 y215 h20 w450 +left BackgroundTrans vVersionInfo,

Gui, Show, Center w470 NA,

CheckVersion()

return

;----------------------------------------------------------
CheckVersion()
{
	UpdateMsg("检查目录")
	myDocPath:=A_MyDocuments "\My Games\Path of Exile"
	if !FileExist(myDocPath)
		FileCreateDir,%myDocPath%
	if !FileExist("ahk")
		FileCreateDir,ahk
	FileAppend ,`r`n;---,%myDocPath%\tempSetting.ahk
	
	UpdateMsg("读取在线版本")
	DownloadFileTo("Version.txt")
	FileRead, webVersionTxt, % fileList["Version.txt"][1]
	RegExMatch(webVersionTxt,"im)(*ANYCRLF)(?<=Version:=).*(`a)?", webVersion)
	RegExMatch(webVersionTxt, "im)(*ANYCRLF)(?<=UpdateInfo:=).*(`a)?", UpdateInfo)
	RegExMatch(webVersionTxt, "im)(*ANYCRLF)(?<=UpdateFile:=).*(`a)?", UpdateFile)
	if !webVersion
	{
		UpdateMsg("读取在线版本失败,即将退出")
		Sleep,5000
		ExitApp
	}
	
	UpdateMsg("读取本地版本")
	FileRead, versionTxt, Version.txt
	RegExMatch(versionTxt, "im)(*ANYCRLF)(?<=Version:=).*(`a)?", localVersion)
	if !localVersion
		localVersion:="0.0.0"
	
	;显示版本信息
	tmsg:="当前版本:" localVersion "--最新版本:" webVersion
	GuiControl,, VersionInfo,%tmsg%
	
	UpdateMsg("检查基础文件")
	for wpath,lpath in fileList
	{
		if(lpath[2]==2 and !FileExist(lpath[1]))
			DownloadFileTo(wpath)
	}
	
	;判断版本是否最新
	if(localVersion==webVersion)
	{
		UpdateMsg("已经是最新版本")
		RunMainAhk()
	}
	else
	{
		tmsg:="当前版本:" localVersion "--最新版本:" webVersion "`n更新说明:" UpdateInfo "`n更新文件列表:" UpdateFile
		MsgBox, 262148,%title% , %tmsg%
		IfMsgBox yes
		{
			UpdateAHK(UpdateFile)
		}
		else
			RunMainAhk()
	}
	FileDelete,fileList["Version.txt"][1]
}

;更新程序
UpdateAHK(UpdateFile)
{
	UpdateMsg("备份文件")
	FileCreateDir,ahk\版本备份%localVersion%
	loop, %A_ScriptDir%\ahk\*.ahk
	{
		FileCopy, %A_LoopFileFullPath%,ahk\版本备份%localVersion%
	}
	
	if(localVersion=="0.0.0")
	{
		for wpath,lpath in fileList
		{
			if(lpath[2]==1)
				DownloadFileTo(wpath)
		}
	}
	else
	{
		t_filelist:=StrSplit(UpdateFile,",")
		loop,% t_filelist.count()
			DownloadFileTo(t_filelist[A_Index])
	}
	FileCopy, % fileList["Version.txt"][1],Version.txt,1
	DownloadFileTo("更新说明.txt")
	
	
	run % fileList["更新说明.txt"][1]
	if (localVersion=="0.0.0")
		run % fileList["说明书.docx"][1]
	RunMainAhk()
}

;下载文件
DownloadFileTo(filename)
{
	localpath:=fileList[filename][1]
	if !localpath
	{
		UpdateMsg(filename "读取文件数组信息失败")
		UpdateMsg("即将退出")
		ExitApp
	}
	webpath:=alipath . filename
	UrlDownloadToFile, %webpath%,%localpath%
	
	if(ErrorLevel)
	{
		UpdateMsg(filename "下载失败")
		UpdateMsg("即将退出")
		Sleep,3000
		ExitApp
	}
	else
	{
		UpdateMsg(filename "下载成功")
	}
}

;运行主程序
RunMainAHK()
{
	if FileExist(mainAhk)
	{
		UpdateMsg("即将运行")
		UpdateMsg("先欣赏几秒加钱通道(点击查看大图)")
		;DownloadFileTo("小广告.txt")
		;run % fileList["小广告.txt"][1]
		Sleep,5000
		Run %mainAhk%
	}
	else
	{
		UpdateMsg("没有找到主程序")
		UpdateMsg("即将退出")
	}
	ExitApp
}

LaunchPic:
Run % fileList["加钱通道-新.png"][1]
return

;更新gui
UpdateMsg(msg)
{
	GuiControlGet,txt,,SplashSubMessage
	txt:=txt "-" msg "`n"
	arr := StrSplit(txt, "-") 
	mI := arr.MaxIndex()		
	If (mI > 10) {
		txt:=""
		loop,9
		{
			txt:=Trim(txt "-" arr[mI - 9 + A_Index])
		}
	}
	Sleep,200
	GuiControl,, SplashSubMessage,%txt%
}

;检查注册表
CheckReg()
{
	AutoHotkeyKey := "SOFTWARE\AutoHotkey"
	RegRead CurrentPath, HKLM, %AutoHotkeyKey%, InstallDir
	if(!CurrentPath)
	{
msg=
(
你看起来可以运行AHK
但是你其实没有安装
点是我自动下载安装
点否我自己删除离开
)
		MsgBox, 262148,%title% ,%msg%
		IfMsgBox yes
		{
			run https://www.autohotkey.com/download/ahk-install.exe
		}
		else
		{
			FileDelete,%A_ScriptFullPath%
			MsgBox,再他妈的见
		}
		ExitApp
	}
}